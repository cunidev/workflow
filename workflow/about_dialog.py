from gi.repository import Gtk
from . import timeutils

class AboutDialog(Gtk.AboutDialog):
    def __init__(self, parent, version):
        Gtk.AboutDialog.__init__(self)


        self.set_program_name("Workflow")
        self.set_comments("A simple screen time monitoring app for Linux, based on ActivityWatch")
        self.set_version(version)

        try:
            self.set_logo(Gtk.IconTheme.get_default().load_icon("com.gitlab.cunidev.Workflow", 128, 0))
        except:
            pass

        self.set_license_type(Gtk.License.GPL_3_0)
        self.set_authors(["Raffaele T. (cunidev) https://gitlab.com/cunidev"])
        self.set_website("https://gitlab.com/cunidev/workflow/")
        self.set_website_label("Project page")
        self.set_title("")

        self.connect('response', self.hide_dialog)
        self.connect('delete-event', self.hide_dialog)

    def hide_dialog(self, widget=None, event=None):
        self.hide()
        return True
